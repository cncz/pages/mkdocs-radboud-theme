.PHONY: serve
serve: venv
	. venv/bin/activate; mkdocs serve --dev-addr 127.0.0.1:8080 --dirtyreload

.PHONY: pages
pages: venv
	. venv/bin/activate; mkdocs build

.PHONY: serve_static
serve_static: pages
	python3 -m http.server 8080 --bind 127.0.0.1 --directory  public/

.PHONY: check
check: linkcheck

.PHONY: linkcheck
linkcheck:
	lychee docs/

.PHONY: venv
venv: venv/touchfile

venv/touchfile: requirements.txt
	test -d venv || python3 -m venv venv
	. venv/bin/activate; pip install -Ur requirements.txt
	touch venv/touchfile

.PHONY: clean
clean:
	rm -rf venv
	rm -rf public/*
